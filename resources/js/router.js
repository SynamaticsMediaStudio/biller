import store from './store'
import Home from './components/Pages/Home.vue';
import Dashboard from './components/Pages/Dashboard.vue';
import Products from './components/Pages/Products/Products.vue';
import Product from './components/Pages/Products/Product.vue';
import CreateProduct from './components/Pages/Products/Create.vue';
import Login from './components/Pages/Auth/Login.vue';
import Orders from './components/Pages/Orders/Orders.vue';
import CreateOrders from './components/Pages/Orders/CreateOrders.vue';

const routes = [
    {
        path:"/login",
        name:"login-page",
        component:Login,
    },
    {
        path:"/",
        name:"home-page",
        component:Home,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/dashboard",
        name:"dashboard",
        component:Dashboard,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/products",
        name:"products",
        component:Products,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/product/:id",
        name:"product",
        component:Product,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/create/product",
        name:"create-product",
        component:CreateProduct,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/orders",
        name:"orders",
        component:Orders,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/create/order",
        name:"create-orders",
        component:CreateOrders,
        meta:{
            Auth:true,
        }
    },
    {
        path:"/clients",
        name:"clients",
        component:CreateProduct,
        meta:{
            Auth:true,
        }
    },
]
export default routes;