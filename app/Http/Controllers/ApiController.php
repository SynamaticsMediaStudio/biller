<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function Index()
    {
        return view('home');
    }
    public function Login(Request $request)
    {
        $request->validate([
            "username"=>"required|email",
            "password"=>"required",
        ]);
        $username = $request->username;
        $password = $request->password;
        $http = new \GuzzleHttp\Client;

        try {
            $response = $http->post(route('passport.token'), [
                'form_params' => [
                    'username'=>$username,
                    'password'=>$password,
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.CLIENT_ID'),
                    'client_secret' => config('services.passport.CLIENT_SECRET'),
                ],
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() == 400) {
                return response()->json(["errors"=>[["Invalid Request. Please Enter a username and/or Password"]]],$e->getCode());
            } elseif($e->getCode() == 401) {
                return response()->json(["errors"=>[["Username and/or Password is Incorrect."]]],$e->getCode());
            } else {
                return response()->json(["errors"=>[["Something went wrong on the server. Please try after a few mins."]]],$e->getCode());
            }
            
        }
    }
}
