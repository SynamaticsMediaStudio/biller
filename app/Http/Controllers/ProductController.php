<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->pagination){
            if($request->pagination = "all"){
                $products = Product::where('user',$request->user()->id)->get();
            }
            else{
                $products = Product::where('user',$request->user()->id)->paginate($request->pagination);
            }
        }
        else {
            $products = Product::where('user',$request->user()->id)->paginate(20);
        }
        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "amount"=>"required",
            "items"=>"required",
            "name"=>"required",
            "taxable"=>"required",
        ]);
        $product = new Product;    
        $product->amount = $request->amount;
        $product->cgst = $request->cgst;
        $product->igst = $request->igst;
        $product->items = $request->items;
        $product->name = $request->name;
        $product->sgst = $request->sgst;
        $product->taxable = $request->taxable;
        $product->user = $request->user()->id;
        $product->save();
        return response()->json(["message"=>"Created new Product","created_id"=>$product->id],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Product $product)
    {
        if($product->user == $request->user()->id){
            return $product;
        }
        else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            "amount"=>"required",
            "created_at"=>"required",
            "id"=>"required",
            "items"=>"required",
            "name"=>"required",
            "taxable"=>"required",
        ]);

        $product->amount = $request->amount;
        $product->cgst = $request->cgst;
        $product->igst = $request->igst;
        $product->items = $request->items;
        $product->name = $request->name;
        $product->sgst = $request->sgst;
        $product->taxable = $request->taxable;
        $product->user = $request->user()->id;
        $product->save();
        return response()->json("Update Complete",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
