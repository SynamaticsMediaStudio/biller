<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Product;
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('user');
            $table->integer('items');
            $table->float('amount', 10, 2);
            $table->boolean('taxable')->default(false);
            $table->float('cgst', 10, 2)->nullable();
            $table->float('sgst', 10, 2)->nullable();
            $table->float('igst', 10, 2)->nullable();
            $table->timestamps();
        });
        $product            = new Product;
        $product->name      = "Default Product";
        $product->user      = 1;
        $product->items     = 2;
        $product->amount    = 300.00;
        $product->taxable   = true;
        $product->cgst      = 18.00;
        $product->sgst      = 18.00;
        $product->igst      = null;
        $product->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
