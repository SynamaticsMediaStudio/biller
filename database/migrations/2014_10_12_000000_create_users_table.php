<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;
use App\User;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->json('options');
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        $user = new User;
        $user->name     = "Administrator";
        $user->email    = "synamatics@gmail.com";
        $user->email_verified_at    = now();
        $user->password = Hash::make("admin@123");
        $user->options  = json_encode([
            'company'=>[
                'name'=>'Synamatics',
                'email'=>'synamatics@gmail.com',
                'address'=>[
                    'line_1'=>'Devaswom Lane',
                    'line_2'=>'Nalanchira PO',
                    'city'=>'Trivandrum',
                    'district'=>'Trivandrum',
                    'state'=>'Kerala',
                    'country'=>'India',
                    'zipcode'=>'695004',
                ],
                "website"=>["url"=>"/"],
                'phone'=>['office'=>'0471000000'],
            ],
            'tax'=>[
                'tax_id'=>'XXXX-XXXX-XXXX-XXXX',
                'type'=>'Service',
                'tax_types'=>[
                    'SGST'=>['tax_name'=>'SGST'],
                    'IGST'=>['tax_name'=>'IGST'],
                    'CGST'=>['tax_name'=>'CGST'],
                ],
            ],
            'settings'=>[
                'theme'=>[
                    'background'=>"#00BCD4",
                    "color"=>"#ffffff",
                ],
                "invoice"=>[
                    "invoice_start_id"=>"0001",
                    "invoice_prefix"=>"SYN",
                    "invoice_sufix"=>"",
                    "invoice_note"=>"Thank you for your purchase.",
                    "invoice_terms"=>""
                ],
                "smtp"=>[
                    "status"=>false,
                    "settings"=>[
                        "incoming"=>[
                            "host"=>"",
                            "port"=>"",
                            "type"=>"",
                            "username"=>"",
                            "password"=>"",
                        ],
                        "outgoing"=>[
                            "host"=>"",
                            "port"=>"",
                            "type"=>"",
                            "username"=>"",
                            "password"=>"",
                        ],
                    ]
                ]
            ],
        ]);
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
